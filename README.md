
#### INTRO
Your mission, should you choose to accept it, involves the development and deployment of a nanoservice.
Please read the following instructions before starting to implement your mission, you don't want to miss any important instruction, especially those in [General Guidelines](#general-guidelines)

#### Preparations: Install and Configure Jenkins

1. Spin up a linux VM, and make sure you setup ssh on it.
1. Setup the LTS release of Jenkins.
1. Make Jenkins available on SSL (port 443).

#### NodeJS/python service
First, clone this repository and checkout to a new branch.
You need to develop a service called counter-service. It should just maintain a counter of the amount of POST requests it served, and return it on every GET request it gets.
A sample NodeJS service named sample-service already exists  [here](services/sample-service/sample.js)
you can also improve this if you wish.

#### Deployment
Create a Jenkins Pipeline Job for the service. It should get the Pipeline script from this repository.
 
The job should get branch name as a parameter, deploy the service (using the branch name), **run it** and make sure it's ready to be used in **production** (see [General Guidelines](#general-guidelines)). 
A sample pipeline groovy script already exists [here](pipeline/counter.groovy). (Please note: samples are not full, and do not contain all relevant details, you're expected to improve them, and add missing tasks. They can also include bugs).

We understand there might be a short service downtime when re-deploying a service, that’s fine.

#### Docker

1. Create a Dockerfile for building your counter-service into a docker image.
1. Add a "build" stage into your pipeline script and deploy the counter-service as a docker container.
1. Connect a jnlp docker Jenkins agent to the Jenkins master.
1. Make sure your pipeline script runs on the jenkins agent and not on the master.

#### Deliverables
A working Jenkins system with a Pipeline deployment job for the counter-service.

A Bitbucket Pull-Request from your branch to master, containing all of the code for this exercise.
The Pull-Request should contain a description of your changes, and any other comment you’d like us to know of.

#### General Guidelines
Your code should be as simple as possible, yet well documented and robust.  
Spend some time on designing your solution. 
Think about operational use cases from the real world. 
What happens if a service crashes? 
How much effort will it take to create a new service?

