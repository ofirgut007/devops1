properties([
        disableConcurrentBuilds(),
        parameters([
                string(defaultValue: '', description: '', name: 'my_branch'),
        ]),
        pipelineTriggers([])
])

node("master") {

    stage('Build') {
        try {
            sh "mkdir service"
            checkout([$class: 'GitSCM', branches: [[name: "${params.my_branch}"]], doGenerateSubmoduleConfigurations: true, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "service"]], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'Change-Me', url: "git remote add origin https://bitbucket.org/ofirgut007/"]]])
	} catch (exc) {
	    error "ERROR: Failed to checkout branch - ${params.my_branch}"
	}
    }

    stage('Deploy') {
        echo 'Deploying...'

    }
}

